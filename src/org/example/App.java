package org.example;

import org.example.core.*;
import org.example.core.factory.ConcreteCreator;
import org.example.core.factory.Game;
import org.example.core.enums.Games;

public class App {

    public static void main(String[] args) {

        Game game = null;
        for (Games games: Games.values()) {
            try{
                System.out.println(games + ": ");
                game = ConcreteCreator.facortyMethod(games,5);              //Create a concrete instance of Game based on enum Games
                if(game != null)
                    game.initializeGame();                                                //Initialize the game
                else{
                    System.out.println("Theres no instance for this type of game");
                }

            }catch (Exception e){
                System.out.println(e);
            }finally{
                game.close();                                                           //Always close the game
            }
        }
    }
}
