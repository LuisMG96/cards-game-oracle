package org.example.core.enums;

public enum Suite {
    HEARTS,
    SPADE,
    CLUBS,
    DIAMONDS;
}