package org.example.core.factory;

import org.example.core.enums.Games;

public class ConcreteCreator {

	/**
	 *
	 * @param gameToPlay Enum the tipe of game that needs to be instantiated
	 * @param numberOfPlayers Number of players that the game needs to have
	 * @return
	 */
	public static Game facortyMethod(Games gameToPlay, Integer numberOfPlayers) {
		switch (gameToPlay){
			case POKER:
				return Poker.getInstance(numberOfPlayers);
			case BLACKJACK:
				return BlackJack.getInstance(numberOfPlayers);
			default:
				return null;
		}
	}
}
