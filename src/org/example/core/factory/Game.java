package org.example.core.factory;

import org.example.core.Card;
import org.example.core.Deck;
import org.example.core.Player;
import org.example.core.exceptions.DeckException;

import java.util.ArrayList;

public abstract class Game implements AutoCloseable {

    //Agregar una lista de jugadores, así podemos controlar las cartas a repartir y los
    protected ArrayList<Player> players;
    protected static Game instance;
    protected Deck deck;
    protected final Integer NUMBER_OF_CARDS;

    /**
     *Initialize the deck and the number of players
     *
     * @param numberOfPlayers number of players to create into the game
     * @param numberOfCards number of cards that the game needs to be play
     */
    protected Game(Integer numberOfPlayers, Integer numberOfCards) {
        deck = new Deck();
        players = new ArrayList<>();
        NUMBER_OF_CARDS = numberOfCards;
        for (int i = 0; i < numberOfPlayers; i++)
            players.add(new Player());

    }

    /**
     * This needs to be called for initialize the game
     * This method will deal cards to each player and finally ç
     * show the hands of each player
     *
     * @throws Exception If the number of players multiplied by the number of cards exceed the cards on deck.
     *
     */
    public void initializeGame() throws Exception {
        dealCardsToPlayers();
        showHandOfPlayers();
    }

    /**
     * This method call {@link #dealCard(Player)} to deal a {@link Card} to each player on {@link #players}
     * this process is done {@link #NUMBER_OF_CARDS} times
     *
     * @throws DeckException If the number of players multiplied by the number of cards exceed the cards on deck.
     */

    private void dealCardsToPlayers() throws Exception {
        for (int i = 0; i < NUMBER_OF_CARDS; i++) {
            for (Player player: players) {
                dealCard(player);
            }
        }
    }

    /**
     * This method will get attribute hand of each #Player in the Game and print it on console
     * */
    private void showHandOfPlayers(){
        for (Player player: players)
            System.out.println(player.getHand());
    }

    /**
     *
     * @param p The player that will receive a card from Deck
     * @throws Exception If the deck is empty or p is null
     */
    private void dealCard(Player p) throws Exception {
        p.setHand(deck.dealCard());
    }

    /**
     * Implementation of close abstract method of Autoclosable Interface
     * Print that the instance is clean and set null to the instance
     */

    public void close() { //tendría que checar que no truene nada
        System.out.println("Cleaning up instance");
        instance = null;
    }

}
