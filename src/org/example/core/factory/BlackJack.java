package org.example.core.factory;

import org.example.core.Deck;
import org.example.core.Player;

import java.util.ArrayList;

public class BlackJack  extends Game {

	/**
	 *
	 * @param numberOfPlayers number of players to be created
	 */
	public BlackJack(Integer numberOfPlayers){
		super(numberOfPlayers, 2);
	}

	/**
	 *
	 * @param numberOfPLayers number of players to be created into the game
	 * @return BlackJack instance
	 */
	public static Game getInstance(Integer numberOfPLayers) {
		if (instance == null) {
			instance = new BlackJack(numberOfPLayers);

		}else{
			System.out.println("Theres one active instance");
		}
		return instance;
	}


}
