package org.example.core.factory;

import org.example.core.Deck;
import org.example.core.Player;

import java.util.ArrayList;

public class Poker extends Game {

	/**
	 *
	 * @param numberOfPlayers Number of players that needs to be created
	 */
	private Poker(Integer numberOfPlayers){
		super(numberOfPlayers, 5);
	}

	/**
	 *
	 * @param numberOfPLayers
	 * @return Poker
	 */
	public static Game getInstance(Integer numberOfPLayers) {
		if (instance == null) {
			instance = new Poker(numberOfPLayers);
		}else{
			System.out.println("Theres one active instance");
		}
		return instance;
	}

}
