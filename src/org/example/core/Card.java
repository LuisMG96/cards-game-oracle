package org.example.core;

public class Card {

    //Agregar setters
    private String suite;
    private int value;

    /**
     *
     * @param value The number of the card
     * @param suite The suite of the card
     */
    public Card(int value, String suite) {
        this.suite = suite;
        this.value = value;
    }
    
    /**
     *
     * @return #String this method use StringBuilder to create a readable message of Card
     */
    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();
        message.append("Card{ suite='");
        message.append(suite);
        message.append("', value=");
        message.append(value);
        message.append("}");

        return message.toString();
    }
}
