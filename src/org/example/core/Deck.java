package org.example.core;

import org.example.core.enums.Suite;
import org.example.core.exceptions.DeckException;

import java.util.ArrayList;

public class Deck {

    private ArrayList<Card> deck = new ArrayList<Card>();
    private ArrayList<Card> delt = new ArrayList<Card>();

    /**
     * Create a deck using the enum #Suite
     */
    public Deck() {
        for(int i = 1 ; i <= 13 ; i++)
            for(Suite suite: Suite.values())
                deck.add(new Card(i,suite.name()));
    }

    /**
     * Randomly take a card of deck, then remove this card from deck and add it to delt
     * @return #Card
     * @throws DeckException When the deck is empty
     * @throws Exception
     */
    public Card dealCard() throws Exception{
        try{
            int rand = (int)(Math.random() * ((deck.size() - 1)));
            Card c = deck.get(rand);
            deck.remove(c);
            delt.add(c);
            return c;
        }catch (IndexOutOfBoundsException e){
            throw new DeckException("The number of cards to deal exceed the quantity of cards in deck");
        }catch (Exception e){
            throw new Exception("Error on dealCard method");
        }
    }
}
