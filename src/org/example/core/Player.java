package org.example.core;

import java.util.ArrayList;

public class Player {

    private ArrayList<Card> hand; //esto debería ser privado

    /**
     * Constructor of Player, intiailize an empty array in hand
     */
    public Player() {
        hand = new ArrayList<>();
    }

    /**
     *
     * @param card The card to be added into hand ArrayList
     * @throws Exception when the Card is null
     */
    public void setHand(Card card) throws Exception{ //Validar que hand no venga nula
        if(card == null)
            throw new Exception("Card mustn't be null");
        this.hand.add(card);
    }

    /**
     *
     * @return #hand property
     */

    public ArrayList getHand(){ return hand; }


}
