package org.example.core.exceptions;

public class DeckException extends Exception {

	/**
	 *
	 * @param message Message to be shown
	 */
	public DeckException(String message){
		super(message);
	}

}
